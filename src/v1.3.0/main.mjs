export function setup(ctx) {
	ctx.onCharacterSelectionLoaded(ctx => {
		// debug
		const debugLog = (...msg) => {
			mod.api.SEMI.log(`${id} v11119`, ...msg);
		};

		// script details
		const id = 'drop-chances';

		const localeSettings = {
			minimumFractionDigits: 0,
			maximumFractionDigits: 4
		};

		// setting groups
		const SETTING_DISPLAY = ctx.settings.section('Display type');
		SETTING_DISPLAY.add(
			[{
				type: 'label',
				label: 'Choose whether to display drop rates as a percentage or as 1/x',
				name: 'label-droprate-style'
			},
			{
				type: 'radio-group',
				name: `droprate-style`,
				label: '',
				default: "x",
				options: [
					{ value: "x", label: "1/x", hint: "" },
					{ value: "%", label: "Percentage", hint: "" },
					{ value: "x%", label: "1/x, %", hint: "" }
				]
			}]
		);

		const formatChance = (value) => value.toLocaleString(undefined, localeSettings);

		const dropDisplay = (weight, totalWeight) => {
			const displayStyle = SETTING_DISPLAY.get("droprate-style");

			if (displayStyle == "x") {
				return ` <span class="text-info">(1/${formatChance(totalWeight / weight)})</span>`;
			} else if (displayStyle == "%") {
				return ` <span class="text-info">(${formatChance((weight / totalWeight) * 100)}%)</span>`;
			} else {
				return ` <span class="text-info">(1/${formatChance(totalWeight / weight)}, ${formatChance((weight / totalWeight) * 100)}%)</span>`;
			}
		};


		const dropChances = () => {
			// Monster Drops
			ctx.patch(CombatManager, 'getMonsterDropsHTML').replace(function (_getMonsterDropsHTML, monster, respectArea) {
				let drops = '';
				let totalWeight = monster.lootTable.totalWeight;
				const hasArea = respectArea && this.selectedArea !== undefined;
				const barrierDustItem = this.game.items.getObjectByID('melvorAoD:Barrier_Dust');

				if (monster.lootChance !== undefined) {
					totalWeight = (totalWeight * 100) / monster.lootChance;
				}

				if (monster.lootChance > 0 && monster.lootTable.size > 0 && !(hasArea && !this.selectedArea.dropsLoot)) {
					drops = monster.lootTable.sortedDropsArray.map((drop) => {
						let dropText = templateLangString('BANK_STRING_40', {
							qty: `${drop.maxQuantity}`,
							itemImage: `<img class="skill-icon-xs mr-2" src="${drop.item.media}">`,
							itemName: drop.item.name,
						});
						dropText += dropDisplay(drop.weight, totalWeight);
						return dropText;
					}).join('<br>');

					drops += `<br><br>Average Value: `;
					const averageDropValue = monster.lootTable.getAverageDropValue();
					averageDropValue.forEach((quantity, currency) => { drops += `<br>${formatChance(quantity)} <img class="skill-icon-xs mr-2" src="${currency.media}">${currency.name}`; });
					drops += `<br>`;
				}

				let always = `${getLangString('MISC_STRING_7')}`;
				if (!(hasArea && !this.selectedArea.dropsCurrency)) {
					monster.currencyDrops.forEach(
						(currencyDrop) => {
							always += `<br>${templateLangString(
								'BETWEEN_VALUE',
								{
									qty1: numberWithCommas(currencyDrop.min),
									qty2: numberWithCommas(currencyDrop.max),
								}
							)} <img class="skill-icon-xs mr-2" src="${currencyDrop.currency.media}">${currencyDrop.currency.name}`;
						}
					);
				}

				let bones = '';
				const hasAlwaysDrops = (monster.bones !== undefined || monster.hasBarrier && barrierDustItem !== undefined);
				if (hasAlwaysDrops && !(hasArea && !this.selectedArea.dropsBones)) {
					if (monster.bones !== undefined) {
						bones += `<br>${ monster.bones.quantity } x <img class="skill-icon-xs mr-2" src="${monster.bones.item.media}">${monster.bones.item.name}`;
					}
					if (monster.hasBarrier && barrierDustItem !== undefined) {
						bones += `<br>${Math.max(Math.floor((Math.floor(monster.levels.Hitpoints * numberMultiplier * (monster.barrierPercent / 100))) / numberMultiplier / 20), 1)} x <img class="skill-icon-xs mr-2" src="${barrierDustItem.media}">${barrierDustItem.name}`;
					}
					bones += `<br><br>`;
				}
				else {
					bones = `<br>${getLangString('COMBAT_MISC_107')}<br><br>`;
				}

				let html = `<span class="text-dark">${always}${bones}<br>`;
				if (drops !== '') {
					html += `${getLangString('MISC_STRING_8')}<br><small>${getLangString('MISC_STRING_9')}</small><br>${drops}`;
				}
				html += '</span>';
				return html;
			});

			// Chest Contents
			window.viewItemContents = (item) => {
				if (!item) {
					item = game.bank.selectedBankItem.item;
				}
				const dropsOrdered = item.dropTable.sortedDropsArray;
				let totalWeight = item.dropTable.totalWeight;
				const drops = dropsOrdered.map((drop) => {
					let dropText = templateString(getLangString('BANK_STRING_40'), {
						qty: `${numberWithCommas(drop.maxQuantity)}`,
						itemImage: `<img class="skill-icon-xs mr-2" src="${drop.item.media}">`,
						itemName: drop.item.name,
					});
					dropText += dropDisplay(drop?.weight, totalWeight);
					return dropText;
				}).join('<br>');

				Swal.fire({
					title: item.name,
					html: getLangString('BANK_STRING_39') + '<br><small>' + drops,
					imageUrl: item.media,
					imageWidth: 64,
					imageHeight: 64,
					imageAlt: item.name,
				});
			};

			// Thieving
			ctx.patch(Thieving, 'fireNPCDropsModal').replace(function (_fireNPCDropsModal, area, npc) {
				const sortedTable = npc.lootTable.sortedDropsArray;
				let html = '<span class="text-dark">';
				npc.currencyDrops.forEach(({ currency, quantity }) => {
					const { min, max } = this.getNPCCurrencyRange(npc, currency, quantity);
					html += `<small><img class="skill-icon-xs mr-2" src="${currency.media}"> ${currency.formatAmount(`${formatNumber(min)} - ${formatNumber(max)}`)}</small><br>`;
				});

				// Common
				html += `${getLangString('THIEVING_POSSIBLE_COMMON')}<br><small>`;
				if (sortedTable.length) {
					html += `${getLangString('THIEVING_MOST_TO_LEAST_COMMON')}<br>`;
					const totalWeight = npc.lootTable.weight;
					html += sortedTable.map(({ item, weight, minQuantity, maxQuantity }) => {
						let text = `${maxQuantity > minQuantity ? `${minQuantity}-` : ''}${maxQuantity} x <img class="skill-icon-xs mr-2" src="${item.media}">${item.name}`;
						text += dropDisplay(weight, totalWeight);
						return text;
					}
					).join('<br>');
					html += `<br>Average Value: `;
					const averageDropValue = npc.lootTable.getAverageDropValue();
					averageDropValue.forEach((quantity, currency) => { html += `<br>${quantity.toFixed(3)} <img class="skill-icon-xs mr-2" src="${currency.media}">${currency.name}`; });
					html += `<br>`;
				}
				else {
					html += getLangString('THIEVING_NO_COMMON_DROPS') + `<br>`;
				}
				html += `</small><br>`;

				// Rare
				html += `${getLangString('THIEVING_POSSIBLE_RARE')}<br><small>`;
				const generalRareHTML = [];
				this.generalRareItems.forEach((drop) => {
					if (this.isCorrectRealmForGeneralRareDrop(drop, npc.realm) && (drop.npcs === undefined || drop.npcs.has(npc)))
						generalRareHTML.push(`${this.formatSpecialDrop(drop.item)}${dropDisplay(drop.chance / 100, 1)}`);
				});
				html += generalRareHTML.join('<br>');
				html += `</small><br><br>`;

				// Area Unique
				if (area.uniqueDrops.length) {
					html += `${getLangString('THIEVING_POSSIBLE_AREA_UNIQUE')}<br><small>`;
					html += area.uniqueDrops.map((drop) => `${this.formatSpecialDrop(drop.item, drop.quantity)}${dropDisplay(this.areaUniqueChance / 100, 1)}`).join('<br>');
					html += '</small><br><br>';
				}

				// NPC Unique
				if (npc.uniqueDrop !== undefined) {
					let chance = this.getNPCPickpocket(npc);
					html += `${getLangString('THIEVING_POSSIBLE_NPC_UNIQUE')}<br><small>${this.formatSpecialDrop(npc.uniqueDrop.item, npc.uniqueDrop.quantity)}${dropDisplay(chance / 100, 1)}</small>`;
				}

				html += '</span>';

				SwalLocale.fire({
					title: npc.name,
					html,
					imageUrl: npc.media,
					imageWidth: 64,
					imageHeight: 64,
					imageAlt: npc.name,
				});
			});

			// Archology
			const sizes = [['tiny', 'artefactsTiny'], ['small', 'artefactsSmall'], ['medium', 'artefactsMedium'], ['large', 'artefactsLarge']];

			ctx.patch(ArtefactDropListElement, 'setList').replace(function (_setList, digSite) {
				for (const [size, elm] of sizes) {
					const drops = digSite.artefacts[size];
					drops.sortedDropsArray.map(({ item, weight, minQuantity, maxQuantity }) => {
						const el = createElement('div');
						el.innerHTML = `${this.formatSpecialDrop(item, maxQuantity)}${dropDisplay(weight, drops.totalWeight)}${this.getWeightBadge(weight)}`;
						this[elm].appendChild(el);
					});
				}
			});

			debugLog(id, 'Mod loaded successfully');
		};

		ctx.onInterfaceReady(() => {
			dropChances();
		});
	});
}